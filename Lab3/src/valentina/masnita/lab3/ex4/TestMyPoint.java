package lab3.ex1.ex4;

public class TestMyPoint {

    public static void main(String[] args) {
        MyPoint a = new MyPoint();
        MyPoint b = new MyPoint(6, 8);

        System.out.println("distance from a to b: " + a.distance(b));
    }

}
