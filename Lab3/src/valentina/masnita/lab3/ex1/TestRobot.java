package lab3.ex1.ex1;

import valentina.masnita.lab3.ex1.Robot;

public class TestRobot {

    public static void main(String[] args) {

        Robot mihai = new Robot(2);
        Robot maria = new Robot(3);

        System.out.println(mihai.toString());
        mihai.printX();

        mihai.change(7);

        System.out.println(mihai.toString());
        mihai.printX();

    }

}
