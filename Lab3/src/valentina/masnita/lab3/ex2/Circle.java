package lab3.ex2;

public class Circle {

    private double radius;
    private String colour;

    public Circle() {
        this.radius = 1;
        this.colour = "red";
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius(double radius) {
        return radius;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public Circle(double radius, String colour) {
        this.radius = radius;
        this.colour = colour;
    }

    public Circle(double radius) {
        this.radius = radius;
        colour = "red";
    }

    public double getRadius() {
        return this.radius;
    }

    public double getArea() {
        return 3.14 * this.radius * this.radius;
    }

}
