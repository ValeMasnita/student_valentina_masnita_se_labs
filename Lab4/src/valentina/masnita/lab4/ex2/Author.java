package valentina.masnita.lab4.ex2;

import org.w3c.dom.ls.LSOutput;

import javax.crypto.spec.PSource;

public class Author {

    private String name;
    private String email;
    private char gender;

    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public char getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString(){
        return "author - " + name + "(" + gender + ") at" + email ;
    }

}
