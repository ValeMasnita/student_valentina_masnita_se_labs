package valentina.masnita.lab4.ex3;

import valentina.masnita.lab4.ex2.Author;

public class Book {

    private String name;
    private Author author;
    private double price;
    private int q;

    public Book(String name, Author author, double price, int qtyInStock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.q = qtyInStock;
    }

    public Book(String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.q = 0;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQ() {
        return q;
    }

    public void setQ(int q) {
        this.q = q;
    }

    @Override
    public String toString() {
        return name + "by" + author.toString();
    }
}
