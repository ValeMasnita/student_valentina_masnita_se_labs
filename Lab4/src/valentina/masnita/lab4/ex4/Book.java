package valentina.masnita.lab4.ex4;

import valentina.masnita.lab4.ex2.Author;

public class Book {

        private String name;
        private Author[] authors;
        private double price;
        private int qtyInStock;

        public Book(String name, Author[] authors, double price) {
            this.name = name;
            this.authors = authors;
            this.price = price;
        }

        public Book(String name, Author[] authors, double price, int qtyInStock) {
            this.name = name;
            this.authors = authors;
            this.price = price;
            this.qtyInStock = qtyInStock;
        }

        public String getName() {
            return (this.name);
        }

        public Author[] getAuthors() {
            return (this.authors);
        }

        public double getPrice() {
            return (this.price);
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public int getQtyInStock() {
            return (this.qtyInStock);
        }

        public void setQtyInStock(int qtyInStock) {
            this.qtyInStock = qtyInStock;
        }

        public String toString() {
            return (this.name + " by " + this.authors.length + " authors");
        }

        public void printAuthors() {
            for (Author a : this.authors) {
                System.out.println(a.toString());
            }
        }

}
