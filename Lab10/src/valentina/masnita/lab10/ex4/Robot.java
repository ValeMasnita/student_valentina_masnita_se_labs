package valentina.masnita.lab10.ex4;

public class Robot extends Thread {
    private int x;
    private int y;
    private boolean destroyed;
    private volatile int [][] harta;

    Robot(int [][] harta) {
        this.x = (int)Math.random() * 100;
        this.y = (int)Math.random() * 100;
        this.destroyed = false;
        this.harta = harta;
    }

    public void updatePos() {
        synchronized(this) {
            if (harta[x][y] == -1) {
                this.destroyed = true;
                harta[x][y] = 0;
            }
            else {
                harta[x][y] = 0;
                this.x = (int)Math.random() * 100;
                this.y = (int)Math.random() * 100;
                if (harta[x][y] == 1) {
                    this.destroyed = true;
                    harta[x][y] = -1;
                }
                else {
                    harta[x][y] = 1;
                }
            }
        }
    }

    @Override
    public void run() {
        try {
            while (!destroyed) {
                updatePos();
                sleep(100);
            }
            System.out.println("robot destroyed");
        }
        catch (InterruptedException e) {
            System.out.println("something went wrong");
        }
    }

    public static void main(String[] args) {
        int [][] harta = new int[100][100];

        Robot r1 = new Robot(harta);
        Robot r2 = new Robot(harta);
        Robot r3 = new Robot(harta);
        Robot r4 = new Robot(harta);
        Robot r5 = new Robot(harta);
        Robot r6 = new Robot(harta);
        Robot r7 = new Robot(harta);
        Robot r8 = new Robot(harta);
        Robot r9 = new Robot(harta);
        Robot r10 = new Robot(harta);

        r1.start();
        r2.start();
        r3.start();
        r4.start();
        r5.start();
        r6.start();
        r7.start();
        r8.start();
        r9.start();
        r10.start();
    }
}