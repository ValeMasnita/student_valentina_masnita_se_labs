package valentina.masnita.lab10.ex6;

import javax.swing.*;

public class Chronometer extends Thread {
    private double time;

    @Override
    public void run() {
        // System.out.println(this.time);
        synchronized(this) {
            try {
                while (true) {
                    // wait();
                    sleep(100);
                    setTime(time + 0.1);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                System.out.println("problem updating time");
            }
            //update time and wait to be notified ?
        }
    }

    synchronized public void setTime(double time) {
        this.time = time;
    }
}