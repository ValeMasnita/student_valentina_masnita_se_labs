package valentina.masnita.lab10.ex3;

public class Counter extends Thread {
    Thread t;
    int i;

    Counter(int i) { this.i = i; this.t = null; }
    Counter(int i, Thread t) { this.i = i; this.t = t; }

    @Override
    public void run() {
        try {
            if (t != null)
                t.join();

            int n = i + 100;
            for (int j = i; j <= n; ++j) {
                // Thread.sleep(1000); for slowed down display
                System.out.println("counter at: " + j);
            }
        }
        catch (Exception e) {
            System.out.println("troubles in thread joining");
        }
    }


    public static void main(String[] args) {
        Counter t1 = new Counter(0);
        Counter t2 = new Counter(100, t1);

        t1.start();
        t2.start();
    }
}