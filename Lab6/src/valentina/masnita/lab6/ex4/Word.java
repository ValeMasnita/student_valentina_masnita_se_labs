package valentina.masnita.lab6.ex4;

public class Word {

    private String name;

    public Word(String name) { this.name = name; }

    public String getWord() { return (this.name); }

}
