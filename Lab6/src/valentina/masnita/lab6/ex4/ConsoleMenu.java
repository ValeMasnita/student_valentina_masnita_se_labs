package valentina.masnita.lab6.ex4;

import java.util.Scanner;

public class ConsoleMenu {

    public static void main(String[] args) {
        Dictionary dex = new Dictionary();
        Scanner in = new Scanner(System.in);
        Word word;
        Definition definition;
        int d = 1;

        while (d != 0) {
            System.out.println("1 - add word;\n2 - show words");
            d = in.nextInt();
            in.nextLine();  // delay the scanner so you can read value properly
            // below
            if (d == 0)
                break ;
            switch (d) {
                case 1: {
                    System.out.println("give the word");
                    word = new Word(in.nextLine());
                    System.out.println("give the definition");
                    definition = new Definition(in.nextLine());
                    dex.addWord(word, definition);
                    break ;
                }
                case 2: {
                    if (dex.getHashMap().size() == 0) {
                        System.out.println("sry no words");
                        break ;
                    }
                    dex.printDictionary();
                    break ;
                }
                default:
                    System.out.println("dude chill");
            }
        }

    }

}
