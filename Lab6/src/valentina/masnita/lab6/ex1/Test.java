package valentina.masnita.lab6.ex1;

public class Test {

    public static void main(String[] args) {
        BankAccount x, y, z;
        x = new BankAccount("A");
        y = new BankAccount("B");
        z = new BankAccount("A", 999);

        System.out.println(x.equals(y) + " " + x.equals(z));
        System.out.println(z.hashCode());
    }

}
