package valentina.masnita.lab6.ex2;

public class Test {

    public static void main(String[] args) {
        Bank banca = new Bank();

        banca.addAccount("A", 1000);
        banca.addAccount("B", 2000);
        banca.addAccount("C", 100);
        banca.addAccount("D", 1300);
        banca.addAccount("E", 4000);

        banca.printAccounts(500, 2000);
    }

}
