package valentina.masnita.colocviu;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Solutie {

    public static void main(String[] args) {
    new Fereastra();

}

    public static class Fereastra extends JFrame {

        String aux;

        Fereastra() {

            super("Colocviu Valentina Masnita");
            setSize(400, 400);
            setResizable(false);
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setLayout(null);

            JTextField t1 = new JTextField();
            JTextField t2 = new JTextField();
            JTextField aux = new JTextField();

            t1.setBounds(10, 10, 100, 50);
            t2.setBounds(10, 100, 100, 50);

            JButton b1 = new JButton("Button");

            t1.setEditable(true);
            t2.setEditable(true);

            b1.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    aux.setText(t2.getText());
                    t2.setText(t1.getText());
                    t1.setText(aux.getText());

                }
            });

            b1.setBounds(100, 200, 100, 100);

            add(t1);
            add(t2);
            add(b1);

            setVisible(true);


        }
}
}
