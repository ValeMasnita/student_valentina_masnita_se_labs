package lab2.ex5;

import java.util.Random;

public class Ex5 {

    public static void main(String[] args) {
        int i;
        Random rand = new Random();
        int[] arr = new int[10];
        for (i = 0; i < 10; i++) {
            arr[i] = rand.nextInt(100);
        }
        bubbleSort(arr, 10);
        printArray(arr, 10);
    }
    static void printArray(int arr[], int size)
    {
        int i;
        for (i = 0; i < size; i++)
            System.out.print(arr[i] + " ");
        System.out.println();
    }
    static void bubbleSort(int arr[], int n)
    {
        int i, j, temp;
        boolean swapped;
        for (i = 0; i < n - 1; i++)
        {
            swapped = false;
            for (j = 0; j < n - i - 1; j++)
            {
                if (arr[j] > arr[j + 1])
                {
                    // swap arr[j] and arr[j+1]
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    swapped = true;
                }
            }

            // IF no two elements were
            // swapped by inner loop, then break
            if (swapped == false)
                break;
        }
    }

}
