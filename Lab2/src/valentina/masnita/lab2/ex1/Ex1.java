package lab2.ex1;
import java.util.Scanner;

public class Ex1 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("enter a number: ");
        int a = scan.nextInt();
        System.out.print("enter another one: ");
        int b = scan.nextInt();
        scan.close();
        System.out.println(Math.max(a, b));
    }
}
