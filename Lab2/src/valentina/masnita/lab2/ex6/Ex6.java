package lab2.ex6;

import java.util.Scanner;

public class Ex6 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("give a number: ");
        int n = scan.nextInt();
        long fact = 1;

        for (int i = 1; i <= n; i++) {
            fact *= i;
        }
        scan.close();
        System.out.println(fact);
    }

}
