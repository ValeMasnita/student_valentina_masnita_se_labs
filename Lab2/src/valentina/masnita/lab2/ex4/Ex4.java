package lab2.ex4;

import java.util.Scanner;

public class Ex4 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("give length of aray: ");
        int n = scan.nextInt();
        int[] arr = new int[n];
        int i;
        for (i = 0; i < n; i++) {
            System.out.println("give " + i + " number: ");
            arr[i] = scan.nextInt();
        }
        int max = arr[0];

        scan.close();
        for (i = 1; i < n; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        System.out.println("max nr is : " + max);
    }

}
