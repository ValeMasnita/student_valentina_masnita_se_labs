package valentina.masnita.lab5.ex4;

import valentina.masnita.lab5.ex3.Light;
import valentina.masnita.lab5.ex3.Temperature;

import java.util.concurrent.TimeUnit;

public class Controller {

    Temperature tsens;
    Light lsens;

    private static Controller cont;

    private Controller() {}

    public static Controller getController() {
        if (cont == null) {
            cont = new Controller();
        }
        return (cont);
    }

    public void	control() {
        int i = 0;

        while (i < 20) {
            tsens = new Temperature();
            lsens = new Light();
            System.out.println("temperature " + tsens.readValue() + " light " + lsens.readValue());
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println(e.getMessage());
            }
            i++;
        }
    }

}
