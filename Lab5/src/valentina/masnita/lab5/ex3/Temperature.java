package valentina.masnita.lab5.ex3;

public class Temperature extends Sensor {

    private int value;

    public Temperature() {
        value = (int)(Math.random() * 100);
    }
    public int	readValue() {
        return (this.value);
    }

}
