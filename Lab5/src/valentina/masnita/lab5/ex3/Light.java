package valentina.masnita.lab5.ex3;

public class Light extends Sensor {

    private int value;

    public Light() {
        value = (int)(Math.random() * 100);
    }
    public int	readValue() {
        return (this.value);
    }

}
