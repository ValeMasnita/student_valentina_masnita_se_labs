package valentina.masnita.lab5.ex3;
import java.util.concurrent.TimeUnit;

public class Controller {

    Temperature	tsens;
    Light lsens;

    public void	control() {
        int i = 0;

        while (i < 20) {
            tsens = new Temperature();
            lsens = new Light();
            System.out.println("temperature " + tsens.readValue() + " light " + lsens.readValue());
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println(e.getMessage());
            }
            i++;
        }
    }

}
