package valentina.masnita.lab5.ex1;

public class Test {

    public static void main(String[] args) {
        Circle cerc = new Circle(3.0, "red", true);
        Rectangle dr = new Rectangle(2, 4, "yellow", true);

        System.out.println(cerc.toString());
        System.out.println(dr.toString());
    }

}
