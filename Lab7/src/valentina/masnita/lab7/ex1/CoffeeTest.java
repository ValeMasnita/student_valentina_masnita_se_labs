package valentina.masnita.lab7.ex1;

public class CoffeeTest {

    public static void main(String[] args) {
        CofeeMaker mk = new CofeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();

        for(int i = 0;i<15;i++){
            Coffee c;
            try {
                c = mk.makeCoffee();
            } catch (TooMuchCoffeeException e) {
                System.out.println("Exception:"+e.getMessage()+" cupsmade:"+e.getCups());
                break ;
            }
            try {
                d.drinkCoffee(c);
            } catch (TemperatureException e) {
                System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
            }
            finally{
                System.out.println("Throw the coffee cup.\n");
            }
        }
    }
}//.class

class CofeeMaker {
    Coffee makeCoffee() throws TooMuchCoffeeException {
        System.out.println("Make a coffee");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        Coffee coffee = new Coffee(t,c);
        if (Coffee.getCups() >= 10)
            throw new TooMuchCoffeeException(Coffee.getCups(), "cant make more");
        return coffee;
    }

}//.class

class Coffee {
    private static int n = 0;
    private int temp;
    private int conc;

    Coffee(int t, int c) {
        n++;
        temp = t;
        conc = c;
    }
    static int getCups() {
        return (n);
    }
    int getTemp(){return temp;}
    int getConc(){return conc;}
    public String toString(){return "[coffee temperature="+temp+":concentration="+conc+"]";}
}//.class

class CoffeeDrinker {
    void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException{
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Coffee is too hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Coffee concentration too high!");
        System.out.println("Drink coffee:"+c);
    }
}//.class

class TemperatureException extends Exception{
    int t;
    public TemperatureException(int t,String msg) {
        super(msg);
        this.t = t;
    }

    int getTemp(){
        return t;
    }
}//.class

class ConcentrationException extends Exception{
    int c;
    public ConcentrationException(int c,String msg) {
        super(msg);
        this.c = c;
    }

    int getConc(){
        return c;
    }
}//.class

class TooMuchCoffeeException extends Exception {
    int n;

    public TooMuchCoffeeException(int n, String msg) {
        super(msg);
        this.n = n;
    }

    public int getCups() { return (this.n); }
}