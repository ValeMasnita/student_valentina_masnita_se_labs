package valentina.masnita.lab7.ex2;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Search {

    public static void main(String[] args) {
        char c;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter character to be searched in data.txt: ");
        c = scanner.next().charAt(0);
        scanner.close();
        int k = 0;

        try {
            FileReader fd = new FileReader("C:\\Users\\esteretse lonenol\\Repository\\student_valentina_masnita_se_labs\\Lab7\\src\\valentina\\masnita\\lab7\\ex2\\data.txt");
            int i;

            while ((i = fd.read()) != -1)
                k+= ((char)i == c ? 1 : 0);
        } catch (FileNotFoundException e) {}

        catch (IOException e) {}

        System.out.println("In data.txt we have " + k + " " + c);
    }

}
