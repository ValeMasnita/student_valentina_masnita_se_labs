package valentina.masnita.lab7.ex3;

import java.io.*;
import java.util.Scanner;

public class Encryption {

    public static void main(String[] args) throws IOException {
        FileReader in = new FileReader("C:\\Users\\esteretse lonenol\\Repository\\student_valentina_masnita_se_labs\\Lab7\\src\\valentina\\masnita\\lab7\\ex3\\data.txt");
        Scanner stdin = new Scanner(System.in);
        System.out.println("Press: E to encrypt and D to decrypt: ");
        String c = stdin.next();
        stdin.close();

        FileWriter out = new FileWriter((String)("C:\\Users\\esteretse lonenol\\Repository\\student_valentina_masnita_se_labs\\Lab7\\src\\valentina\\masnita\\lab7\\ex3\\data." + (c.equals("E") ? "enc" : "dec")));

        int i;
        char x;
        while ((i = in.read()) != -1) {
            x = (char)i;
            if (i != 10 && i != 32)
                x = (char)(c.equals("E") ? (i - 1) : (i + 1));
            out.write(x);
        }

        in.close();
        out.close();
    }

}
