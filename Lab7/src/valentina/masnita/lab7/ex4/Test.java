package valentina.masnita.lab7.ex4;

import java.util.Scanner;

public class Test {

    public static void main(String[] args) throws Exception {
        Manage manager = new Manage();
        Scanner in = new Scanner(System.in);
        String model;
        double price;
        int opt;

        while (true) {
            System.out.println("1. add car\n2. see car details\n");
            opt = in.nextInt();
            switch (opt) {
                case 1: {
                    System.out.println("give the car model: ");
                    in.nextLine();
                    model = in.nextLine();
                    System.out.println("give price: ");
                    price = in.nextDouble();
                    manager.addCar(model, price);
                    break ; }
                case 2: {
                    System.out.println("give the model you are looking for: ");
                    in.nextLine();
                    model = in.nextLine();
                    manager.getCar(model);
                    break ;}
                default:
                    break ;
            }
        }
    }

}
