package valentina.masnita.lab7.ex4;

import java.io.*;

public class Manage {

    public Manage() {}

    public void addCar(String model, double price) throws IOException {
        Car car = new Car(model, price);

        ObjectOutputStream o =
                new ObjectOutputStream(
                        new FileOutputStream("C:\\Users\\esteretse lonenol\\Repository\\student_valentina_masnita_se_labs\\Lab7\\src\\valentina\\masnita\\lab7\\ex4\\cars\\" + model));

        o.writeObject(car);
    }

    public void getCar(String model) throws IOException, ClassNotFoundException {
        ObjectInputStream in =
                new ObjectInputStream(
                        new FileInputStream("C:\\Users\\esteretse lonenol\\Repository\\student_valentina_masnita_se_labs\\Lab7\\src\\valentina\\masnita\\lab7\\ex4\\cars\\" + model));

        Car car = (Car)in.readObject();
        System.out.println(car);
    }

}
