package valentina.masnita.lab11.ex2;

public class Monitor {

    // update for when the quantity changes
    public void update(String name, int value) {
        System.out.println("item " + name + " has now only " + value + " items left");
    }

    // update for the removal of an item
    public void update(String name) {
        System.out.println("item " + name + " has been removed");
    }

    // update for the addition of an item
    public void update(Product product) {
        System.out.println(product.getName() + " has been added to the stock");
    }

}
