package valentina.masnita.lab11.ex2;

public class Test {

    public static void main(String[] args) {
        Monitor monitor = new Monitor();

        Stock stock = new Stock(monitor);

        stock.addProduct(new Product("Bluza", 100, 30, monitor));
        stock.addProduct(new Product("Tricou", 100, 20, monitor));
        stock.addProduct(new Product("Geaca", 100, 120, monitor));
        stock.addProduct(new Product("Pantaloni", 100, 80, monitor));
        stock.addProduct(new Product("Secrete", 100, 100, monitor));
        stock.seeProducts();
        stock.getProduct("Tricou").setQuantity(150);
        stock.removeProduct("Secrete");
        stock.seeProducts();
    }

}
