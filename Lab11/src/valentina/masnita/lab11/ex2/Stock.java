package valentina.masnita.lab11.ex2;

import java.util.ArrayList;

public class Stock {
    private Monitor monitor;
    private ArrayList<Product> products;

    Stock(Monitor monitor) {
        this.monitor = monitor;
        products = new ArrayList<Product>();
    }

    public void seeProducts() {
        for (Product product : this.products) {
            System.out.println(product);
        }
    }

    public void addProduct(Product product) {
        products.add(product);
        monitor.update(product);
    }

    public void removeProduct(String name) {
        this.products.remove(getProduct(name));
        monitor.update(name);
    }

    public Product getProduct(String name) {
        for (Product product : this.products) {
            if (product.getName().equals(name)) {
                return (product);
            }
        }
        return (null);
    }
}