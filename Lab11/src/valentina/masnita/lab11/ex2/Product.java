package valentina.masnita.lab11.ex2;

public class Product {

    private String name;
    private int quantity;
    private double price;
    private Monitor monitor;

    Product(String name, int quantity, double price, Monitor monitor) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.monitor = monitor;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
        monitor.update(name, quantity);
    }

    public String getName() {
        return (this.name);
    }

    @Override
    public String toString() {
        return ("product " + this.name + " is priced at " + this.price + " and there are only " + this.quantity + "left");
    }

}
