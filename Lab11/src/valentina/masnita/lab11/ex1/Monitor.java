package valentina.masnita.lab11.ex1;

public class Monitor {

    public void update(double value) {
        System.out.println("Change in temperature; temperature: " + value);
    }

}
