package valentina.masnita.lab11.ex1;

public class Test {

    public static void main(String[] args) {
        Monitor monitor = new Monitor();
        TemperatureSensor temp = new TemperatureSensor(monitor);

        temp.start();

    }

}
